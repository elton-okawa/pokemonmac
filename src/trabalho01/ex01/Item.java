package trabalho01.ex01;

public class Item {
	private String name;
	private int heal;
	private boolean available = true;
	
	public Item(String name, int heal){
		this.name = name;
		this.heal = heal;
	}
	
	public String getName(){
		return name;
	}
	
	public int getHeal(){
		available = false;;
		return heal;
	}
	
	public boolean getStatus(){
		return available;
	}

}
