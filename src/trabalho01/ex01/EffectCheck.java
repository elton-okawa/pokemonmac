package trabalho01.ex01;

public class EffectCheck {
	
	public static final int NORMAL = 0;
	public static final int FIGHT = 1;
	public static final int FLYING = 2;
	public static final int POISON = 3;
	public static final int GROUND = 4;
	public static final int ROCK = 5;
	
	private static final float[][] TYPE_CHART = 
		{{1, 1, 1, 1, 1, 0.5f},
		{2, 1, 0.5f, 0.5f, 1, 2},
		{1, 2, 1, 1, 1, 0.5f},
		{1, 1, 1, 0.5f, 0.5f, 0.5f},
		{1, 1, 0, 2, 1, 2},
		{1, 0.5f, 2, 1, 0.5f, 1}};
		
	
	public static boolean checkEffect(Pokemon pAttack, Pokemon pDefense){
		float effect = TYPE_CHART[pAttack.getType()][pDefense.getType()];
		if (effect != 1){
			return true;
		}
		else {
			return false;
		}
	}
	
	public static int getDamage(Pokemon pAttack, int atkIndex, Pokemon pDefense){
		float effect = TYPE_CHART[pAttack.getType()][pDefense.getType()];
		return Math.round((pAttack.getAttack(atkIndex).getDamage() * effect));
	}
	
	public static String getEffectText(Pokemon pAttack, Pokemon pDefense){
		float value = TYPE_CHART[pAttack.getType()][pDefense.getType()];
		if (value == 2f){
			return "Super Efetivo";
		}
		else if (value == 0.5f){
			return "Não foi muito efetivo";
		}
		else {
			return "Nem deu dano";
		}
	}
}
