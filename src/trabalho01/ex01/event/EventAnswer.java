package trabalho01.ex01.event;

import trabalho01.ex01.GameController;
import trabalho01.ex01.Trainer;

public class EventAnswer extends Event {

	private Trainer tFirstAction;
	private Trainer tSecondAction;
	private GameController gc;
	private int eventId2;
	
	private String log = "";
	
	public EventAnswer(long eventTime, GameController gc, int eventId2, Trainer tFirstAction, Trainer tSecondAction){
		super(eventTime);
		this.tFirstAction = tFirstAction;
		this.tSecondAction = tSecondAction;
		this.gc = gc;
		this.eventId2 = eventId2;
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
		int eventId;
		if (tSecondAction.getCurrentPokemon().getAlive()){
			eventId = eventId2;
		}
		else {
			eventId = tSecondAction.getNextEvent(tFirstAction.getNumberOfPokemonsAlive());	
		}
		Event ev = createEvent(eventId, tSecondAction, tFirstAction);
		gc.addEvent(ev);
		
		switch (eventId){
			case Event.LOSE:
				log = tSecondAction.getName() + " está sem pokemons";
				break;
			case Event.RUN:
				log = tSecondAction.getName() + " saiu correndo que nem louco";
				break;
			default:
				log = "Vez do " + tSecondAction.getName();
				gc.addEvent(new EventRound(GameController.getTime(), gc));
				break;
		}
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return log;
	}

}
