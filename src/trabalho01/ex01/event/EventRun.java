package trabalho01.ex01.event;

import trabalho01.ex01.Trainer;


public class EventRun extends Event{
	
	private Trainer runningTrainer;
	private Trainer standingTrainer;
	
	public EventRun(long eventTime, Trainer runningTrainer, Trainer standingTrainer) {
		// TODO Auto-generated constructor stub
		super(eventTime);
		this.runningTrainer = runningTrainer;
		this.standingTrainer = standingTrainer;
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return "Treinador " + runningTrainer.getName() + " fugiu de " + standingTrainer.getName() + System.lineSeparator();
	}
	
}
