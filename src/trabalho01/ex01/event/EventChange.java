package trabalho01.ex01.event;

import trabalho01.ex01.Trainer;

public class EventChange extends Event{
	private Trainer currentTrainer;
	private String previousPoke;

	public EventChange(long eventTime, Trainer currentTrainer) {
		// TODO Auto-generated constructor stub
		super(eventTime);
		this.currentTrainer = currentTrainer;
		previousPoke = currentTrainer.getCurrentPokemon().getName();
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
//		Sempre troca pelo pokemon imediatamente posterior.
		currentTrainer.setCurrentPokemon(currentTrainer.getNextPokemon());
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return "Treinador " + currentTrainer.getName() + " trocou " + previousPoke + " por " + currentTrainer.getCurrentPokemon().getName() + System.lineSeparator();
	}

}
