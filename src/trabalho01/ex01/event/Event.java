package trabalho01.ex01.event;

import trabalho01.ex01.GameController;
import trabalho01.ex01.Trainer;

abstract public class Event {

	public static final int ATTACK = 0;
	public static final int ITEM = 1;
	public static final int CHANGE = 2;
	public static final int RUN = 3;
	public static final int LOSE = 4;
	public static final int CAPTURE = 5;
	
	private long evtTime;
	
	public Event(long eventTime) {
		evtTime = eventTime;
	}
	
	public boolean ready() {
		return System.currentTimeMillis() >= evtTime;
	}
	
	public void setEventTime(long evtTime){
		this.evtTime = evtTime;
	}
	
	abstract public void action();
	
	abstract public String description();
	
	protected Event createEvent(int eventId, Trainer t1, Trainer t2){
		Event ev = null;
		switch (eventId){
			case (Event.LOSE):
				ev = new EventLose(GameController.getTime(), t1, t2);
				break;
			case (Event.ATTACK):
				ev = new EventAttack(GameController.getTime(), t1.getCurrentPokemon(), t1.getNextAttack(), t2.getCurrentPokemon());
				break;
			case (Event.RUN):
				ev = new EventRun(GameController.getTime(), t1, t2);
				break;
			case (Event.ITEM):
				ev = new EventItem(GameController.getTime(), t1);
				break;
			case (Event.CHANGE):
				ev = new EventChange(GameController.getTime(), t1);
				break;
		}
		return ev;
	}
}
	