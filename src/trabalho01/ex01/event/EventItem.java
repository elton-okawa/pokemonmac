package trabalho01.ex01.event;

import trabalho01.ex01.Item;
import trabalho01.ex01.Pokemon;
import trabalho01.ex01.Trainer;

public class EventItem extends Event{
	
	private Trainer t;
	private String log = "";
	
	public EventItem(long eventTime, Trainer t) {
		// TODO Auto-generated constructor stub
		super(eventTime);
		this.t = t;
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
		Item item = t.getItem(t.getNextItem());
		int plusHP = item.getHeal();
		Pokemon pokemon = t.getCurrentPokemon();
		int effectiveHeal = pokemon.getMaxHp() - pokemon.getHp();
		// O pokemon não pode ultrapassar o hp máximo;
		if (effectiveHeal > plusHP){
			effectiveHeal = plusHP;
		}
		pokemon.cure(effectiveHeal);
		
		log = t.getName() + " usou o " + item.getName() + " e curou "  + effectiveHeal + " de HP do " + pokemon.getName()+ System.lineSeparator();
		log += pokemon.getName() + " tem " + pokemon.getHp() + " de HP" + System.lineSeparator();
		
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return log;
	}

}

