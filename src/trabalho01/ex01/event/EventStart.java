package trabalho01.ex01.event;

import trabalho01.ex01.GameController;

public class EventStart extends Event{

	private GameController gc;
	
	public EventStart(long eventTime, GameController gc) {
		super(eventTime);
		this.gc = gc;
	}

	@Override
	public void action() {
		gc.addEvent(new EventRound(GameController.getTime(), gc));
	}
 
	@Override
	public String description() {
		return "Game Start" + System.lineSeparator();
	}

}
