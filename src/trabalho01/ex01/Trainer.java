package trabalho01.ex01;

import java.util.ArrayList;
import java.util.Random;

import trabalho01.ex01.event.Event;
import trabalho01.ex01.event.EventAttack;

public class Trainer {
		
	private String name;
	private int player;
	private ArrayList<Pokemon> pokemons;
	private ArrayList<Item> itens;
	private int currentPokemon;
	private Random rand;
	
	public Trainer(String name, int player, ArrayList<Pokemon> pokemons, ArrayList<Item> itens, int currentPokemon){
		this.name = name;
		this.player = player;
		this.pokemons = pokemons;
		this.itens = itens;
		this.currentPokemon = currentPokemon;
		rand = new Random();
	}
	
	public int getNextAttack(){
		int valor = rand.nextInt(getCurrentPokemon().getNumberOfAttacks());
//		System.out.println(getCurrentPokemon().getName() + " " + valor);
		return valor;
//		return (int) Math.random() * getCurrentPokemon().getNumberOfAttacks();
	}
	
	
	public int getNextEvent(int enemyPokemonNumberAlive){
		if(!getCurrentPokemon().getAlive()){
			int nextPokemon = getNextPokemon();
			if(nextPokemon == -1){
				return Event.LOSE;
			}
			else if (enemyPokemonNumberAlive - 2 >= getNumberOfPokemonsAlive()){
				return Event.RUN;
			}
			else { 
//				this.currentPokemon = nextPokemon;
				return Event.CHANGE;
			}
		}
		else if (enemyPokemonNumberAlive - 2 >= getNumberOfPokemonsAlive()){
			return Event.RUN;
		}
		else if (getCurrentPokemon().getHp() < getCurrentPokemon().getMaxHp() / 2 &&
				getNextItem() != -1){
			return Event.ITEM;
		}
		else if (getCurrentPokemon().getHp() < getCurrentPokemon().getMaxHp() / 2){
			int nextPokemon = getHealthierPokemon();
			if (nextPokemon != -1){
				return Event.CHANGE;
			}
		}
		
		
//		Por enquanto Trainer utiliza item quando hp do pokemon chega na metade ou menos
//		if(pokemons.get(currentPokemon).getHp() <= pokemons.get(currentPokemon).getMaxHp() / 2)
//			return Event.ITEM;
		return Event.ATTACK;
	}
	
	public int getNumberOfPokemonsAlive(){
		int counter = 0;
		for (Pokemon pokemon : pokemons){
			if (pokemon.getAlive()){
				counter++;
			}
		}
		return counter;
	}
	
	public int getNextPokemon(){
		int maxHp = 0;
		int index = -1;
		int firstAlive = -1;
		for (int i = 0; i < pokemons.size(); i++){
			Pokemon pokemon = pokemons.get(i);
			int hp = pokemon.getHp();
			if (i != currentPokemon && pokemon.getAlive()){
				firstAlive = i;
				if (hp > pokemon.getMaxHp() / 2){
					if (hp > maxHp){
						maxHp = hp;
						index = i;
					}
				}
			}
		}
		if (index != -1){
			return index;
		}
		else{
			return firstAlive;
		}
	}
	
	public int getHealthierPokemon(){
		// Se não tiver mais pokemons retorna -1
		int maxHp = 0;
		int index = -1;
		int firstAlive = -1;
		for (int i = 0; i < pokemons.size(); i++){
			Pokemon pokemon = pokemons.get(i);
			int hp = pokemon.getHp();
			if (i != currentPokemon && pokemon.getAlive() && hp > pokemon.getMaxHp() / 2){
				if (hp > maxHp){
					maxHp = hp;
					index = i;
				}
			}
		}
		return index;
	}
	
	public int getNextItem(){
		for (int i = 0; i < itens.size(); i++){
			if (itens.get(i).getStatus()){
				return i;
			}
		}
		return -1;
	}
	
	public Item getItem(int index){
		return itens.get(index);
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getPlayer(){
		return this.player;
	}
	
	public ArrayList<Pokemon> getPokemons(){
		return this.pokemons;
	}
	
	public ArrayList<Item> getItens(){
		return this.itens;
	}
	
	public void setCurrentPokemon(int currentPokemon){
		this.currentPokemon = currentPokemon;
	}
	
	public Pokemon getCurrentPokemon(){
		return pokemons.get(currentPokemon);
	}
	
	public int getIndexCurrentPokemon(){
		return this.currentPokemon;
	}
	
}
