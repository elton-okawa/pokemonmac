package trabalho01.ex01;

import java.util.ArrayList;

public class Pokemon {
	private String name;
	private int hp;
	private int maxHp;
	private int type;
	private boolean alive;
	private ArrayList<Attack> attackList;
	
	public Pokemon (String name, int hp, int type, ArrayList<Attack> attackList){
		this.name = name;
		this.hp = hp;
		this.maxHp = hp;
		this.type = type;
		this.attackList = attackList;
		this.alive = true;
	}
	
	public void damage(int value){
		hp -= value;
		if (hp <= 0){
			alive = false;
			hp = 0;
		}
	}
	
	public void cure(int value){
		if (alive && hp < maxHp){
			hp += value;
			if (hp > maxHp) hp = maxHp;
		}
	}
	
	public String getStatus(){
		return name + " tem " + hp + " de hp" + System.lineSeparator();
	}
	
	public boolean getAlive(){
		return alive;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getNumberOfAttacks(){
		return attackList.size();
	}
	
	public Attack getAttack(int index){
		return attackList.get(index);
	}
	
	public int getHp(){
		return this.hp;
	}
	
	public int getMaxHp(){
		return this.maxHp;
	}
	
	public int getType(){
		return type;
	}
}
