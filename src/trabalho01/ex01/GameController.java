package trabalho01.ex01;

import trabalho01.ex01.event.*;

public class GameController {
	
	private static long time = System.currentTimeMillis();
	
	private Trainer t1;
	private Trainer t2;
	private EventSet es = new EventSet();
	
	private int counter = -1; // Devido ao eventStart

	public void addEvent(Event c) {
		es.add(c);
	}

	public void run() {
		Event e;
		while ((e = es.getNext()) != null) {
			if (e.ready()) {
				e.action();
				System.out.println(e.description());
				es.removeCurrent();
//				counter++;
//				if (counter >= 2){
//					counter = 0;
//					addEvent(new EventRound(getTime(), this));
//				}
			}
			
		}
	}
	
	public static long getTime(){
		time += 100;
		return time;
	}
	
	public void setTrainers(Trainer t1, Trainer t2){
		this.t1 = t1;
		this.t2 = t2;
	}
	
	public Trainer getTrainerOne(){
		return t1;
	}
	
	public Trainer getTrainerTwo(){
		return t2;
	}
}
