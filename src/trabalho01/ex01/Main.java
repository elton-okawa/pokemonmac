package trabalho01.ex01;

import java.util.ArrayList;
import java.util.Arrays;

import trabalho01.ex01.event.EventStart;

public class Main {
	public static void main (String args[]){
		GameController gameController = new GameController();
		
		Attack a1 = new Attack("Iron Tail", 10);
		Attack a2 = new Attack("ThunderBolt", 20);
		Attack a3 = new Attack("MegaPunch", 20);
		Attack a4 = new Attack("Scratch", 5);
		Attack a5 = new Attack("Guihottine", 10);
		Attack a6 = new Attack("Stomp", 10);
		Attack a7 = new Attack("Headbutt", 10);
		Attack a8 = new Attack("MegaKick", 20);
				
		Pokemon p1 = new Pokemon("Pikachu", 50, EffectCheck.NORMAL, new ArrayList<Attack>(Arrays.asList(a1,a2,a3,a4)));
		Pokemon p2 = new Pokemon("Jigglypuff", 25, EffectCheck.NORMAL, new ArrayList<Attack>(Arrays.asList(a5,a6)));
		Pokemon p3 = new Pokemon("Geodude", 50, EffectCheck.ROCK, new ArrayList<Attack>(Arrays.asList(a7,a8)));
		Pokemon p4 = new Pokemon("Easy", 100, EffectCheck.FIGHT, new ArrayList<Attack>(Arrays.asList(a2,a5)));
		Pokemon p5 = new Pokemon("Outro", 50, EffectCheck.FLYING, new ArrayList<Attack>(Arrays.asList(a3,a8)));
		Pokemon p6 = new Pokemon("Weezing", 100, EffectCheck.POISON, new ArrayList<Attack>(Arrays.asList(a4,a5)));
		
		Item it1 = new Item("Potion", 10);
		Item it2 = new Item("Super Potion", 20);
		
		Trainer t1 = new Trainer("Ash", 1, new ArrayList<Pokemon>(Arrays.asList(p1, p2, p6)), new ArrayList<Item>(Arrays.asList(it1)), 0);
		Trainer t2 = new Trainer("Brock", 1, new ArrayList<Pokemon>(Arrays.asList(p3, p4, p5)), new ArrayList<Item>(Arrays.asList(it1, it1, it2)), 0);
	
		gameController.setTrainers(t1, t2);
		
//		long tm = System.currentTimeMillis();
		gameController.addEvent(new EventStart(GameController.getTime(), gameController));
//		gameController.addEvent(new EventShowData());
		gameController.run();
	}
}
