package trabalho01.ex02;

import trabalho01.ex02.event.*;

public class GameController extends Controller{
	
	private static long time = System.currentTimeMillis();
	
	private Trainer t1;
	private Trainer t2;
	private Trainer wild;
	private Map map = new Map();
	private EventSet es = new EventSet();
	
	public static long getTime(){
		time += 50;
		return time;
	}
	
	public Map getMap(){
		return map;
	}
	
	public void setTrainers(Trainer t1, Trainer t2){
		this.t1 = t1;
		this.t2 = t2;
	}
	
	public void setWild(Trainer wild){
		this.wild = wild;
	}
	
	public Trainer getTrainerOne(){
		return t1;
	}
	
	public Trainer getTrainerTwo(){
		return t2;
	}
}
