package trabalho01.ex02.item;

public class Potion extends Item {

	private int heal;
	
	public Potion(String name, int quantity, int heal) {
		super(name, quantity);
		this.heal = heal;
		// TODO Auto-generated constructor stub
	}

	public int getHeal(){
		this.quantity -= 1;
		return heal;
	}

}
