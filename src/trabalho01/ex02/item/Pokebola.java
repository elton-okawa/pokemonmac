package trabalho01.ex02.item;

public class Pokebola extends Item{

	private float captureRate;
	
	public Pokebola(String name, int quantity, float captureRate) {
		super(name, quantity);
		this.captureRate = captureRate;
		// TODO Auto-generated constructor stub
	}
	
	public float getCaptureRate(){
		this.quantity -= 1;
		return this.captureRate;
	}

}
