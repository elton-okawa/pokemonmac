package trabalho01.ex02.item;

public class Item {
	private String name;
	protected int quantity;
	
	public Item(String name, int quantity){
		this.name = name;
		this.quantity = quantity;
	}
	
	public String getName(){
		return name;
	}
	
	public int getQuantity(){
		return quantity;
	}
	
	public boolean getStatus(){
		if (quantity > 0){
			return true;
		}
		else {
			return false;
		}
	}

}
