package trabalho01.ex02.event;

import trabalho01.ex02.GameController;
import trabalho01.ex02.Pokemon;
import trabalho01.ex02.Trainer;

public class EventCapture extends Event{
	
	private Trainer t1;
	private Trainer t2;
	private String log;
	
	public EventCapture(long eventTime, Trainer t1, Trainer t2) {
		super(eventTime);
		this.t1 = t1;
		this.t2 = t2;

		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {
		Pokemon wild = t2.getCurrentPokemon();
		if (!t1.isMaxPokemon()){
			String newName = wild.getName() + "Capture";
			t1.addPokemon(wild);
			log = t1.getName() + " capturou " + t2.getCurrentPokemon().getName() + " e mudou seu nome para " + newName;
			wild.setName(newName);
		}
		else {
			String newName = wild.getName() + "Capture";
			log = t1.getName() + " capturou " + t2.getCurrentPokemon().getName() + " e mudou seu nome para " + newName + System.lineSeparator();
			log += t1.getName() + " j� possui o n� m�ximo de pokemons " + t2.getCurrentPokemon().getName() + " est� no centro pokemon" + System.lineSeparator();
			wild.setName(newName);
		}
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return log;
	}

}
