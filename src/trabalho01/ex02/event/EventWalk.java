package trabalho01.ex02.event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import trabalho01.ex02.Attack;
import trabalho01.ex02.EffectCheck;
import trabalho01.ex02.GameController;
import trabalho01.ex02.Map;
import trabalho01.ex02.Pokemon;
import trabalho01.ex02.Position;
import trabalho01.ex02.Trainer;
import trabalho01.ex02.item.Item;

public class EventWalk extends Event{

	private Trainer t1;
	private Trainer t2;
	private GameController gc;
	private String log;
	private ArrayList<Pokemon> wildPokemons;
	private Random rand;
	
	public EventWalk(long eventTime, Trainer t1, Trainer t2, GameController gc) {
		super(eventTime);
		this.t1 = t1;
		this.t2 = t2;
		this.gc = gc;
		
		rand = new Random();
	
		Pokemon p1 = new Pokemon("Wild1", 40, EffectCheck.GROUND, new ArrayList<Attack>(Arrays.asList(new Attack("attackOne", 5))));
		Pokemon p2 = new Pokemon("Wild2", 50, EffectCheck.POISON, new ArrayList<Attack>(Arrays.asList(new Attack("attackTwo", 10))));
		Pokemon p3 = new Pokemon("Wild3", 60, EffectCheck.ROCK, new ArrayList<Attack>(Arrays.asList(new Attack("attackThree", 15))));
		Pokemon p4 = new Pokemon("Wild4", 70, EffectCheck.NORMAL, new ArrayList<Attack>(Arrays.asList(new Attack("attackFour", 20))));
		
		wildPokemons = new ArrayList<>(Arrays.asList(p1, p2, p3, p4));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {
		Map map = gc.getMap();
		log = t1.getName() + " andou de (" + t1.getPosition().getX() + ", " + t1.getPosition().getY() + ") para ("; 
		t1.move();
		log += t1.getPosition().getX() + ", " + t1.getPosition().getY() + ")" + System.lineSeparator();
		log += map.printMap(gc.getTrainerOne(), gc.getTrainerTwo());

//		t.move();

	
		if (map.findPokemon(t1.getPosition())){
//			Pokemon p = new Pokemon("Wild", 80, EffectCheck.NORMAL, new ArrayList<Attack>(Arrays.asList(new Attack("Surprise", 10))));
			Pokemon p = wildPokemons.get(getNextWild());
			Trainer t3 = new Trainer("Wild", new ArrayList<Pokemon>(Arrays.asList(p)), new ArrayList<Item>(), 0, null, Trainer.WILD);
			gc.setWild(t3);
			Event ev = new EventStart(GameController.getTime(), t1, t3, gc);
			gc.addEvent(ev);
			log += System.lineSeparator() + t1.getName() + " encontrou o pokemon " + p.getName() + System.lineSeparator();
		}
		else if (t1.getPosition().equalsTo(t2.getPosition())){
			gc.addEvent(new EventStart(GameController.getTime(), t1, t2, gc));
		}
		else {
			gc.addEvent(new EventWalk(GameController.getTime(), t2, t1, gc));
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return log;
	}
	
	private int getNextWild(){
		return rand.nextInt(wildPokemons.size());
	}

}
