package trabalho01.ex02.event;

import trabalho01.ex02.Trainer;

public class EventLose extends Event{
	private Trainer loser;
	private Trainer winner;
	
	public EventLose(long eventTime, Trainer loser, Trainer winner) {
		// TODO Auto-generated constructor stub
		super(eventTime);
		this.loser = loser;
		this.winner = winner;		
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return loser.getName() + " foi derrotado por " + winner.getName() + System.lineSeparator();
	}
	
}
