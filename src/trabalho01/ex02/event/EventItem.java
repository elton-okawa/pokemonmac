package trabalho01.ex02.event;

import java.util.Random;

import trabalho01.ex02.GameController;
import trabalho01.ex02.Pokemon;
import trabalho01.ex02.Trainer;
import trabalho01.ex02.item.Item;
import trabalho01.ex02.item.Pokebola;
import trabalho01.ex02.item.Potion;

public class EventItem extends Event{
	
	private Trainer t1;
	private Trainer t2;
	private String log = "";
	
	public EventItem(long eventTime, Trainer t1, Trainer t2) {
		// TODO Auto-generated constructor stub
		super(eventTime);
		this.t1 = t1;
		this.t2 = t2;
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
		Item item = t1.getItem(t1.getNextItem());
		if (item instanceof Potion){
			int plusHP = ((Potion) item).getHeal();
			Pokemon pokemon = t1.getCurrentPokemon();
			int effectiveHeal = pokemon.getMaxHp() - pokemon.getHp();
			// O pokemon não pode ultrapassar o hp máximo;
			if (effectiveHeal > plusHP){
				effectiveHeal = plusHP;
			}
			pokemon.cure(effectiveHeal);
			
			log = t1.getName() + " usou o " + item.getName() + " e curou "  + effectiveHeal + " de HP do " + pokemon.getName()+ System.lineSeparator();
			log += pokemon.getName() + " tem " + pokemon.getHp() + " de HP" + System.lineSeparator();
		}
		else if (item instanceof Pokebola){
			Pokemon p2 = t2.getCurrentPokemon();
			float rateHpLost = (1 - p2.getHp()/(float) p2.getMaxHp());
			float totalCaptureChange = rateHpLost * ((Pokebola) item).getCaptureRate();
			Random rand = new Random();
			log = t1.getName() + " usou a " + item.getName();
			if (t2.getNature() == Trainer.WILD){
				if (rand.nextFloat() < totalCaptureChange){
//					log += " e capturou " + p2.getName();
					t2.setCaptured(true);
				}
				else {
					log += " e falhou miseravelmente" + System.lineSeparator();
				}
			}
			else {
				log += " n�o pode capturar o pokemon dos outros" + System.lineSeparator();
			}
				
		}
		
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return log;
	}

}

