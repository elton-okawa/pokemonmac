package trabalho01.ex02.event;

import trabalho01.ex02.GameController;
import trabalho01.ex02.Trainer;

public class EventAnswer extends Event {

	private Trainer tFirstAction;
	private Trainer tSecondAction;
	private GameController gc;
	private int eventId2;
	
	private String log = "";
	
	public EventAnswer(long eventTime, GameController gc, int eventId2, Trainer tFirstAction, Trainer tSecondAction){
		super(eventTime);
		this.tFirstAction = tFirstAction;
		this.tSecondAction = tSecondAction;
		this.gc = gc;
		this.eventId2 = eventId2;
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
		int eventId;
		if (tSecondAction.getCaptured()){
			eventId = tSecondAction.getNextEvent(tFirstAction.getNumberOfPokemonsAlive());
		}
		else if (tSecondAction.getCurrentPokemon().getAlive()){
			eventId = eventId2;
		}
		else {
			eventId = tSecondAction.getNextEvent(tFirstAction.getNumberOfPokemonsAlive());	
		}
		Event ev = createEvent(eventId, tSecondAction, tFirstAction);
		gc.addEvent(ev);
		
		switch (eventId){
			case Event.LOSE:
				log = tSecondAction.getName() + " est� sem pokemons";
				if (tSecondAction.getNature() == Trainer.WILD){
					gc.addEvent(new EventWalk(GameController.getTime(), gc.getTrainerOne(), gc.getTrainerTwo(), gc));
				}
				break;
			case Event.RUN:
				log = tSecondAction.getName() + " saiu correndo que nem louco";
				break;
			case Event.CAPTURE:
				gc.addEvent(new EventWalk(GameController.getTime(), gc.getTrainerOne(), gc.getTrainerTwo(), gc));
				break;
			default:
				log = "Vez do " + tSecondAction.getName();
				gc.addEvent(new EventRound(GameController.getTime(), tFirstAction, tSecondAction, gc));
				break;
		}
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return log;
	}

}
