package trabalho01.ex02.event;

import trabalho01.ex02.GameController;
import trabalho01.ex02.Trainer;

public class EventRound extends Event{

	private GameController gc;
	private Trainer t1;
	private Trainer t2;
	private String log;
	
	public EventRound(long eventTime, Trainer t1, Trainer t2, GameController gc) {
		super(eventTime);
		this.gc = gc;
		this.t1 = t1;
		this.t2 = t2;
	}

	@Override
	public void action() {
//		Trainer t1 = gc.getTrainerOne();
//		Trainer t2 = gc.getTrainerTwo();
//		Event v1 = t1.getNextEvent();
		int eventId1 = t1.getNextEvent(t2.getNumberOfPokemonsAlive());
		
//		Event v2 = t2.getNextEvent();
		int eventId2 = t2.getNextEvent(t1.getNumberOfPokemonsAlive());
//		long time = System.currentTimeMillis();
		
		if (checkEventPriority(eventId1, eventId2)){
			setUpNextEvent(eventId1, eventId2, t1, t2);
//			Event nextEvent = createEvent(eventId1, t1, t2);
//			gc.addEvent(nextEvent);
//			if (!(eventId1 == Event.RUN || eventId1 == Event.LOSE)){
//				gc.addEvent(new EventAnswer(GameController.getTime(), gc, t1, t2));		
//			}
//			log = t1.getName();
		}
		else {
			setUpNextEvent(eventId2, eventId1, t2, t1);
//			Event nextEvent = createEvent(eventId2, t1, t2);
////			v1.setEventTime(time + 200);
//			gc.addEvent(nextEvent);
////			gc.addEvent(v1);
//			if (!(eventId1 == Event.RUN || eventId1 == Event.LOSE)){
//				gc.addEvent(new EventAnswer(GameController.getTime(), gc, t2, t1));
//			}
//			log = t2.getName();
		}
	}

	@Override
	public String description() {
		return log;
	}

	private boolean checkEventPriority(int event1, int event2){
		// check if v1 has more priority than v2
		if (event1 >= event2){
			return true;
		}
		else {
			return false;
		}
	}
	
	private void setUpNextEvent(int eventId1, int eventId2, Trainer tFirst, Trainer tSecond){
		Event nextEvent = createEvent(eventId1, tFirst, tSecond);
		gc.addEvent(nextEvent);
		switch (eventId1){
			case Event.LOSE:
				log = tFirst.getName() + " est� sem pokemons";
				break;
			case Event.RUN:
				log = tFirst.getName() + " saiu correndo que nem louco";
				break;
			default:
				log = "Round start" + System.lineSeparator() + System.lineSeparator() + "Vez do " + tFirst.getName();
				gc.addEvent(new EventAnswer(GameController.getTime(), gc, eventId2, tFirst, tSecond));		
				break;
		}
	}
}
