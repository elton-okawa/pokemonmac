package trabalho01.ex02.event;

import trabalho01.ex02.GameController;
import trabalho01.ex02.Trainer;

public class EventStart extends Event{

	private GameController gc;
	private Trainer t1;
	private Trainer t2;
	
	public EventStart(long eventTime, Trainer t1, Trainer t2, GameController gc) {
		super(eventTime);
		this.gc = gc;
		this.t1 = t1;
		this.t2 = t2;
	}

	@Override
	public void action() {
		gc.addEvent(new EventRound(GameController.getTime(), t1, t2, gc));
	}
 
	@Override
	public String description() {
		return "Game Start" + System.lineSeparator();
	}

}
