package trabalho01.ex02.event;

import trabalho01.ex02.Attack;
import trabalho01.ex02.EffectCheck;
import trabalho01.ex02.Pokemon;

public class EventAttack extends Event{

	private Pokemon pAttack;
	private Pokemon pDefense;
	private int atkIndex;
	private int totalDamage;
	
	public EventAttack(long eventTime, Pokemon pAttack, int atkIndex, Pokemon pDefense) {
		super(eventTime);
		this.pAttack = pAttack;
		this.pDefense = pDefense;
		this.atkIndex = atkIndex;
		totalDamage = EffectCheck.getDamage(pAttack, atkIndex, pDefense);
	}

	@Override
	public void action() {
		pDefense.damage(totalDamage);
	}

	@Override
	public String description() {
		Attack attack = pAttack.getAttack(atkIndex);
		String desc = pAttack.getName() + " usou " + attack.getName() + System.lineSeparator();
		if (EffectCheck.checkEffect(pAttack, pDefense)){
			desc += "Causando " + totalDamage + " de dano" + System.lineSeparator();
			desc += EffectCheck.getEffectText(pAttack, pDefense);
		}
		else {
			desc += "Causando " + totalDamage + " de dano";
		}
		desc += System.lineSeparator();
		desc += pAttack.getStatus();
		desc += pDefense.getStatus();
//		" causando " + attack.getDamage() + " de dano";
		return desc;
	}

}
