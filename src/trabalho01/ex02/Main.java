package trabalho01.ex02;

import java.util.ArrayList;
import java.util.Arrays;

import trabalho01.ex02.event.EventStart;
import trabalho01.ex02.event.EventWalk;
import trabalho01.ex02.item.Item;
import trabalho01.ex02.item.Pokebola;
import trabalho01.ex02.item.Potion;

public class Main {
	public static void main (String args[]){
		GameController gameController = new GameController();
		
		Attack a1 = new Attack("Iron Tail", 10);
		Attack a2 = new Attack("ThunderBolt", 20);
		Attack a3 = new Attack("MegaPunch", 20);
		Attack a4 = new Attack("Scratch", 5);
		Attack a5 = new Attack("Guihottine", 10);
		Attack a6 = new Attack("Stomp", 10);
		Attack a7 = new Attack("Headbutt", 10);
		Attack a8 = new Attack("MegaKick", 20);
		Attack a9 = new Attack("C�lculoIII", 20);
		Attack a10 = new Attack("F�sicaIII", 20);
				
		Pokemon p1 = new Pokemon("Pikachu", 50, EffectCheck.NORMAL, new ArrayList<Attack>(Arrays.asList(a1,a2,a3,a4)));
		Pokemon p2 = new Pokemon("Jigglypuff", 25, EffectCheck.NORMAL, new ArrayList<Attack>(Arrays.asList(a5,a6)));
		Pokemon p3 = new Pokemon("Geodude", 50, EffectCheck.ROCK, new ArrayList<Attack>(Arrays.asList(a7,a8)));
		Pokemon p4 = new Pokemon("Easy", 100, EffectCheck.FIGHT, new ArrayList<Attack>(Arrays.asList(a2,a5)));
		Pokemon p5 = new Pokemon("Outro", 50, EffectCheck.FLYING, new ArrayList<Attack>(Arrays.asList(a3,a8)));
		Pokemon p6 = new Pokemon("Weezing", 100, EffectCheck.POISON, new ArrayList<Attack>(Arrays.asList(a4,a5)));
		Pokemon p7 = new Pokemon("DP", 120, EffectCheck.POISON, new ArrayList<Attack>(Arrays.asList(a9,a10)));
		
		Potion it1 = new Potion("Potion", 1, 10);
		Pokebola pokebola = new Pokebola("Pokebola", 4, 0.8f);
		Potion it2 = new Potion("Super Potion", 1, 20);
		
		Trainer t1 = new Trainer("Ash", new ArrayList<Pokemon>(Arrays.asList(p1, p2, p3, p4)), new ArrayList<Item>(Arrays.asList(it1, pokebola)), 0, new Position(3,3));
		Trainer t2 = new Trainer("Brock", new ArrayList<Pokemon>(Arrays.asList(p3, p5, p6, p7)), new ArrayList<Item>(Arrays.asList(pokebola, it1, it2)), 0, new Position(0,0));
	
		gameController.setTrainers(t1, t2);
		
//		long tm = System.currentTimeMillis();
//		gameController.addEvent(new EventStart(GameController.getTime(), gameController));
		gameController.addEvent(new EventWalk(GameController.getTime(), t1, t2, gameController));
//		gameController.addEvent(new EventWalk(GameController.getTime(), t2, gameController));
//		gameController.addEvent(new EventShowData());
		gameController.run();
	}
}
