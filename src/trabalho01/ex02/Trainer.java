package trabalho01.ex02;

import java.util.ArrayList;
import java.util.Random;

import trabalho01.ex01.event.Event;
import trabalho01.ex01.event.EventAttack;
import trabalho01.ex02.item.Item;

public class Trainer {
		
	public static final int NORMAL = 0;
	public static final int WILD = 1;
	
	private static final int MAX_POKEMONS = 6; 
	
	private String name;
	private ArrayList<Pokemon> pokemons;
	private ArrayList<Item> itens;
	private int currentPokemon;
	private Random rand;
	private Position pos;
	private int nature = Trainer.NORMAL;
	private boolean captured = false;
	
	public Trainer(String name, ArrayList<Pokemon> pokemons, ArrayList<Item> itens, int currentPokemon, Position pos, int nature){
		this(name, pokemons, itens, currentPokemon, pos);
		this.nature = nature;
	}
	
	public Trainer(String name, ArrayList<Pokemon> pokemons, ArrayList<Item> itens, int currentPokemon, Position pos){
		this.name = name;
		this.pokemons = pokemons;
		this.itens = itens;
		this.currentPokemon = currentPokemon;
		this.pos = pos;
		rand = new Random();
	}
	
	public void addPokemon(Pokemon pokemon){
		pokemons.add(pokemon);
	}
	
	public void move(){
		int x = pos.getX();
		int y = pos.getY();
		int plusX = rand.nextInt(3) - 1;
		int plusY = rand.nextInt(3) - 1;
		if (plusX + x < 0){
			pos.setX(0);
		}
		else if (plusX + x > Map.MAP_X - 1){
			pos.setX(Map.MAP_X - 1);
		}
		else {
			pos.setX(x + plusX);
		}
		if (plusY + y < 0){
			pos.setY(0);
		}
		else if (plusY + y > Map.MAP_Y - 1){
			pos.setY(Map.MAP_Y - 1);
		}
		else {
			pos.setY(y + plusY);
		}
	}
	
	public int getNextAttack(){
		int valor = rand.nextInt(getCurrentPokemon().getNumberOfAttacks());
//		System.out.println(getCurrentPokemon().getName() + " " + valor);
		return valor;
//		return (int) Math.random() * getCurrentPokemon().getNumberOfAttacks();
	}
	
	
	public int getNextEvent(int enemyPokemonNumberAlive){
		if (captured){
			return Event.CAPTURE;
		}
		else if(!getCurrentPokemon().getAlive()){
			int nextPokemon = getNextPokemon();
			if(nextPokemon == -1){
				return Event.LOSE;
			}
			else if (enemyPokemonNumberAlive - 2 >= getNumberOfPokemonsAlive() && nature == Trainer.NORMAL){
				return Event.RUN;
			}
			else { 
//				this.currentPokemon = nextPokemon;
				return Event.CHANGE;
			}
		}
		else if (enemyPokemonNumberAlive - 2 >= getNumberOfPokemonsAlive() && nature == Trainer.NORMAL){
			return Event.RUN;
		}
		else if (getCurrentPokemon().getHp() < getCurrentPokemon().getMaxHp() / 2 &&
				getNextItem() != -1){
			return Event.ITEM;
		}
		else if (getCurrentPokemon().getHp() < getCurrentPokemon().getMaxHp() / 2){
			int nextPokemon = getHealthierPokemon();
			if (nextPokemon != -1){
				return Event.CHANGE;
			}
		}
		
		
//		Por enquanto Trainer utiliza item quando hp do pokemon chega na metade ou menos
//		if(pokemons.get(currentPokemon).getHp() <= pokemons.get(currentPokemon).getMaxHp() / 2)
//			return Event.ITEM;
		return Event.ATTACK;
	}
	
	public int getNumberOfPokemonsAlive(){
		int counter = 0;
		for (Pokemon pokemon : pokemons){
			if (pokemon.getAlive()){
				counter++;
			}
		}
		return counter;
	}
	
	public boolean isMaxPokemon(){
		if (pokemons.size() >= MAX_POKEMONS){
			return true;
		}
		else {
			return false;
		}
	}
	public int getNextPokemon(){
		int maxHp = 0;
		int index = -1;
		int firstAlive = -1;
		for (int i = 0; i < pokemons.size(); i++){
			Pokemon pokemon = pokemons.get(i);
			int hp = pokemon.getHp();
			if (i != currentPokemon && pokemon.getAlive()){
				firstAlive = i;
				if (hp > pokemon.getMaxHp() / 2){
					if (hp > maxHp){
						maxHp = hp;
						index = i;
					}
				}
			}
		}
		if (index != -1){
			return index;
		}
		else{
			return firstAlive;
		}
	}
	
	public int getHealthierPokemon(){
		// Se não tiver mais pokemons retorna -1
		int maxHp = 0;
		int index = -1;
		int firstAlive = -1;
		for (int i = 0; i < pokemons.size(); i++){
			Pokemon pokemon = pokemons.get(i);
			int hp = pokemon.getHp();
			if (i != currentPokemon && pokemon.getAlive() && hp > pokemon.getMaxHp() / 2){
				if (hp > maxHp){
					maxHp = hp;
					index = i;
				}
			}
		}
		return index;
	}
	
	public int getNextItem(){
		for (int i = 0; i < itens.size(); i++){
			if (itens.get(i).getStatus()){
				return i;
			}
		}
		return -1;
	}
	
	public Item getItem(int index){
		return itens.get(index);
	}
	
	public String getName(){
		return this.name;
	}
	
	public ArrayList<Pokemon> getPokemons(){
		return this.pokemons;
	}
	
	public ArrayList<Item> getItens(){
		return this.itens;
	}
	
	public void setCurrentPokemon(int currentPokemon){
		this.currentPokemon = currentPokemon;
	}
	
	public Pokemon getCurrentPokemon(){
		return pokemons.get(currentPokemon);
	}
	
	public int getIndexCurrentPokemon(){
		return this.currentPokemon;
	}
	
	public Position getPosition(){
		return pos;
	}
	
	public void setCaptured(boolean type){
		this.captured = type;
	}
	
	public boolean getCaptured(){
		return captured;
	}
	
	public int getNature(){
		return nature;
	}

	
}
