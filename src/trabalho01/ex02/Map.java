package trabalho01.ex02;

import java.util.Random;

public class Map {
	
	private static final int GRASS = 1;
	private static final int GROUND = 0;
	private static final String GRASS_MAP = "~";
	private static final String GROUND_MAP = "-";
	private static final String TRAINER_1 = "1";
	private static final String TRAINER_2 = "2";
	private static final String BATTLE_VS = "X";
	
	private Random rand = new Random();
	
	public static int MAP_X = 4;
	public static int MAP_Y = 4;
	private final int[][] map =
		{{0, 0, 0, 0},
		{0, 0, 0, 0},
		{0, 0, 1, 1},
		{0, 0, 1, 1}};
	
//	public Map(int x, int y){
//		this.x = x;
//		this.y = y;
//	}
	

	
	public int[][] getMap(){
		return map;
	}
	
	public String printMap(Trainer t1, Trainer t2){
//	public String printMap(Trainer t1){
		String mapString = "";
		Position t1Pos = t1.getPosition();
		Position t2Pos = t2.getPosition();
		for (int i = 0; i < map.length; i++){
			for (int j = 0; j < map[0].length; j++){
				if (i == t1Pos.getY() && i == t2Pos.getY() && j == t1Pos.getX() && j == t2Pos.getX()){
					mapString += BATTLE_VS + " ";
				}
				else if (i == t1Pos.getY() && j == t1Pos.getX()){
					mapString += TRAINER_1 + " ";
				}
				else if (i == t2Pos.getY() && j == t2Pos.getX()){
					mapString += TRAINER_2 + " ";
				}
				else if (map[i][j] == GRASS){
					mapString += GRASS_MAP + " ";
				}
				else if (map[i][j] == GROUND){
					mapString += GROUND_MAP + " ";
				}

			}
			mapString += System.lineSeparator();
		}
		return mapString;
	}
	
	public boolean findPokemon(Position pos){
		int place = map[pos.getX()][pos.getY()];
		switch (place){
			case GRASS:
				if (rand.nextInt(2) == 1){
					return true;
				}
			default:
				return false;
		}
	}
}
